import React, { PureComponent } from 'react';
import './Persons.css'
import Person from './Person/Person'

class Persons extends PureComponent {

  // static getDerivedStateFromProps(props, state) {
  //     console.log("[Persons.js] getDerivedStateFromProps");
  //     return false;
  // }

  // componentWillReceiveProps(props) {
  //   console.log("[Persons.js] componentWillReceiveProps");
  // }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log("[Persons.js] shouldComponentUpdate");
  //   if(nextProps.persons !== this.props.persons 
  //     || nextProps.clicked !== this.props.clicked
  //     || nextProps.changed !== this.props.changed)
  //   return true;
  //   else
  //   return false
  // }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("[Persons.js] getSnapshotBeforeUpdate");
    return { message: "Snapshot !" };
  }

  // componentWillUpdate() {
  //   console.log("[Persons.js] componentWillUpdate");
  // }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("[Persons.js] componentDidUpdate");
    console.log({ prevProps: prevProps, prevState, prevState, snapshot: snapshot })
  }

  componentWillUnmount() {
    console.log("[Persons.js] componentWillUnmount");
  }

  render() {
    console.log("[Persons.js] rendering");
    return (
      <div className="Persons">
        {this.props.persons.map((person, index) => {
          return <Person
            key={person.id}
            name={person.name}
            age={person.age}
            click={() => this.props.clicked(index)}
            changed={(event) => this.props.changed(event, person.id)} />
        })}
      </div>
    )
  }
}

export default Persons;