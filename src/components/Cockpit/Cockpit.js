import React , { useEffect, useRef, useContext } from 'react';
import classes from './Cockpit.module.css';
import AuthContext from '../../context/auth-context'


const Cockpit = props => {
    const toggleButttonRef = useRef(null);

    const authContext = useContext(AuthContext);

    console.log("[Cockpit.js] rendering");

    useEffect(() => {
      console.log("[Cockpit.js] useEffect");
      toggleButttonRef.current.click();
      return () => {
        console.log('[Cockpit.js] cleanup work in useEffect');
      }
    }, []);

    useEffect(() => {
      console.log("[Cockpit.js] 2nd useEffect");
      return () => {
        console.log('[Cockpit.js] cleanup work in 2nd useEffect');
      }
    });

    const assignedClasses = [];
    let btnClass = '';

    if(props.showPersons) {
        btnClass = classes.Red;
    }

    if(props.personsLength <= 2) {
      assignedClasses.push(classes.red);
    }
  
    if(props.personsLength <= 1) {
      assignedClasses.push(classes.bold);
    } 

    return (
    <div className={classes.Cockpit}>
       <h1>Hi, I'm a React App</h1>
        <p className={assignedClasses.join(' ')}>This is really working</p>
        <button ref={toggleButttonRef} className={btnClass} onClick={props.clicked} >Toggle Persons</button>
          {authContext.authenticated ? null : <button onClick={authContext.login}>Log In</button>}
    </div>  
)};

export default React.memo(Cockpit);