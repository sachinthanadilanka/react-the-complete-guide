import React, { Component } from 'react';
import classes from './App.module.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass'
import Aux from '../hoc/Aux'
import AuthContext from '../context/auth-context'


class App extends Component {

  constructor(props) {
    super(props);
    console.log("[App.js] constructor");
    this.state = {
      personsState: {
        persons: [
          { id: 1000, name: 'Max', age: 30 },
          { id: 2000, name: "Sachin", age: 33 },
          { id: 3000, name: "Jaani", age: 32 },
          { id: 4000, name: "Thinu", age: 3 }
        ]
      },
      otherState: 'some Other value',
      showPersons: false,
      showCockpit: true,
      changeCounter: 0,
      authenticated: false
    }
  }

  static getDerivedStateFromProps(props, state) {
    console.log("[App.js] getDerivedStateFromProps", props)
    return state
  }

  componentDidUpdate() {
    console.log("[App.js] componentDidUpdate")
  }

  componentDidMount() {
    console.log("[App.js] componentDidMount")
  }

  shouldComponentUpdate() {
    console.log("[App.js] shouldComponentUpdate")
    return true;
  }

  togglePersonsHandler = (event) => {
    this.setState({
      personsState: {
        persons: this.state.personsState.persons,
      },
      showPersons: !this.state.personsState.showPersons
    })
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.personsState.persons];
    persons.splice(personIndex, 1);
    this.setState({
      personsState: {
        persons: persons
      },
      showPersons: this.state.personsState.showPersons
    });
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.personsState.persons.findIndex(p => {
      return p.id === id
    });

    // since it is a good practice to not to mutate the original state object we will use above index to
    // get a copy of the persons object using the spread operator, we can use spread operator to get a
    // copy of any object
    const person = {
      ...this.state.personsState.persons[personIndex]
    };

    // below is an alternative way either

    // const person = Object.assign({}, personsState.persons[personIndex]);

    // now we will modify the copied object instead of the original object

    person.name = event.target.value;
    const persons = [...this.state.personsState.persons];
    persons[personIndex] = person;
    const showPersons = { ...this.state.personsState.showPersons };

    this.setState((prevState, props) => {
      return {
        personsState: {
          persons: persons
        },
        showPersons: showPersons,
        changeCounter: prevState.changeCounter + 1
      }
    })
  }

  toggleCockpit = event => {
    const showCockpit = !this.state.showCockpit
    this.setState({ showCockpit: showCockpit })
  }

  loginHandler = () => {
    this.setState({
      authenticated: true 
    });
  }
 
  render() {
    console.log("[App.js] render");

    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <Persons persons={this.state.personsState.persons}
          clicked={this.deletePersonHandler}
          changed={this.nameChangedHandler} 
          isAuthenticated={this.state.authenticated} />
      );
    }

    let cockpit = null;
    if (this.state.showCockpit === true) {
      cockpit = 
        <Cockpit showPersons={this.state.showPersons}
        personsLength={this.state.personsState.persons.length}
        clicked={this.togglePersonsHandler} />;
      
    }

    return (
      <Aux classes={classes.App}>
        <button onClick={this.toggleCockpit}>Toggle Cockpit</button>
        <AuthContext.Provider value={{
          // below will update the context object always when the state.authenticated is changed.
          authenticated: this.state.authenticated,
          login: this.loginHandler
        }}>
        {cockpit}
        {persons}
        </AuthContext.Provider>
      </Aux>
    );
  }

}

export default withClass(App, classes.App);
